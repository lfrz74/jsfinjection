package com.mitocode.dao;

import java.util.ArrayList;
import java.util.List;

import com.mitocode.model.Curso;

public class CursoDAO implements IDAO{
	
	@Override
	public List<Curso> listar() {
		List<Curso> lista = new ArrayList<>();
		Curso cur;
		for (int i = 0; i < 100; i++) {
			cur = new Curso();
			cur.setIdCurso(i);
			cur.setNombre("JEE 8");
			cur.setHoras(32 + i);
			lista.add(cur);
		}
		return lista;
	}

}
