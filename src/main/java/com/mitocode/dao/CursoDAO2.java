package com.mitocode.dao;

import java.util.ArrayList;
import java.util.List;

import com.mitocode.model.Curso;

public class CursoDAO2 implements IDAO {
	
	@Override
	public List<Curso> listar() {
		List<Curso> lista = new ArrayList<>();
		Curso cur;
		for (int i = 0; i < 200; i++) {
			cur = new Curso();
			cur.setIdCurso(i);
			cur.setNombre("Angular 6");
			cur.setHoras(32 + i);
			lista.add(cur);
		}
		return lista;
	}
}
