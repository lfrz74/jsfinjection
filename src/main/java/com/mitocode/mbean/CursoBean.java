package com.mitocode.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.mitocode.dao.CursoDAO2;
import com.mitocode.model.Curso;
import com.mitocode.service.CursoService;

@Named
@ViewScoped
public class CursoBean implements Serializable {

	private List<Curso> lista;
	private Curso curso;
	private CursoService service;
	
	public CursoBean() {
		lista = new ArrayList<Curso>();
		service = new CursoService(new CursoDAO2());
		this.listar();
	}

	public List<Curso> getLista() {
		return lista;
	}

	public void setLista(List<Curso> lista) {
		this.lista = lista;
	}

	public void listar() {
		this.lista = service.listar();
	}

	public void enviar(Curso cur) {
		this.setCurso(cur);
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}
}
